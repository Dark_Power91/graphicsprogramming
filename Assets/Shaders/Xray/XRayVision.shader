﻿Shader "Hidden/XRayVision"
{
	SubShader
	{
		Tags{ "Queue" = "Transparent" }
		ZTest Always
		ZWrite Off
		Blend one one

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#pragma enable_d3d11_debug_symbols

			struct appdata
			{
				float4 vertex : POSITION;
			};
	
			struct v2f
			{
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)	
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}

			half4 _OverDrawColor;
			half4 _OcclusionColor;
			uniform float3 _MousePosition;

			float _MinRadius;
			float _MaxRadius;

			fixed4 frag(v2f i) : SV_Target
			{
				float d = distance(_MousePosition.xy,i.vertex);
				return lerp(_OverDrawColor, _OcclusionColor, smoothstep(_MinRadius,_MaxRadius, d));
			}
			ENDCG
		}
	}
}
