﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BlitMaterial : MonoBehaviour
{
    [SerializeField]
    private Material _material;

    private Material _myMat;

    void Awake()
    {
        _myMat = new Material(_material);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, _myMat);
    }
}