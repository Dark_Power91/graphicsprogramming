﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BWEffect : MonoBehaviour
{
    [SerializeField]
    [Range (0.0f,1.0f)]
    private float _intensity;
    [SerializeField]
    private bool _autoShift;

    private Material _material;

    void Awake()
    {
        _material = new Material(Shader.Find("Hidden/BWDiffuse"));
    }

    void Update()
    {
        if(_autoShift)
        {
            _intensity = (Mathf.Sin(Time.time) + 1) / 2;
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (_intensity == 0)
        {
            Graphics.Blit(source, destination);
            return;
        }

        _material.SetFloat("_bwBlend", _intensity);
        Graphics.Blit(source, destination, _material);
    }
}
