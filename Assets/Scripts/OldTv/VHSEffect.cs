﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class VHSEffect : MonoBehaviour
{
    [Header("Noise")]
    [SerializeField]
    private Texture _Noise;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _Intensity = 0.64f;

    [Header("Vertical Shift")]
    [SerializeField]
    [Range(0.0f,1.0f)]
    private float _VerticalShiftProbability;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _MaxVerticalShift = 0.5f;

    [Header("Color Shift")]
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _ColorShiftProbability;
    [SerializeField]
    [Range(0.0f, 0.05f)]
    private float _RecoverSpeed = 0.005f;
    [SerializeField]
    [Range(0.0f,0.15f)]
    private float _MinColorShift = 0.003f;
    [SerializeField]
    [Range(0.0f, 0.15f)]
    private float _MaxColorShift = 0.1f;
    
    [Header("Distortion")]
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _DistortionProbability;
    [SerializeField]
    [Tooltip("Higher the value, lower the Max distortion amplitude")]
    private float _BaseDistortion = 480.0f;

    private Material _material;

    void Awake()
    {
        _material = new Material(Shader.Find("Hidden/VHSEffect"));
        _material.SetTexture("_SecondaryTex", _Noise);
        _material.SetFloat("_OffsetPosY", 0f);
        _material.SetFloat("_OffsetColor", 0.001f); 
        _material.SetFloat("_OffsetDistortion", _BaseDistortion);
        _material.SetFloat("_Intensity", _Intensity);
    }

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        // TV noise
        _material.SetFloat("_Intensity", _Intensity);
        _material.SetFloat("_OffsetNoiseX", Random.Range(0f, 0.6f));
        float offsetNoise = _material.GetFloat("_OffsetNoiseY");
        _material.SetFloat("_OffsetNoiseY", offsetNoise + Random.Range(-0.03f, 0.03f));

        // Vertical shift
        float offsetPosY = _material.GetFloat("_OffsetPosY");
        if (offsetPosY > 0.0f)
        {
            _material.SetFloat("_OffsetPosY", offsetPosY - Random.Range(0f, offsetPosY));
        }
        else if (offsetPosY < 0.0f)
        {
            _material.SetFloat("_OffsetPosY", offsetPosY + Random.Range(0f, -offsetPosY));
        }
        else if (CanExecute(_VerticalShiftProbability))
        {
            _material.SetFloat("_OffsetPosY", Random.Range(-_MaxVerticalShift, _MaxVerticalShift));
        }

        // Channel color shift
        float offsetColor = _material.GetFloat("_OffsetColor");
        if (offsetColor > 0.003f)
        {
            _material.SetFloat("_OffsetColor", offsetColor - _RecoverSpeed);
        }
        else if (CanExecute(_ColorShiftProbability))
        {
            _material.SetFloat("_OffsetColor", Random.Range(_MinColorShift, _MaxColorShift));
        }

        // Distortion
        if (CanExecute(_DistortionProbability))
        {
            _material.SetFloat("_OffsetDistortion", Random.Range(1f, _BaseDistortion));
        }
        else
        {
            _material.SetFloat("_OffsetDistortion", _BaseDistortion);
        }

        Graphics.Blit(source, destination, _material);
    }

    private bool CanExecute(float Threshold)
    {
        return Random.Range(0.0f, 1.0f) <= Threshold;
    }
}