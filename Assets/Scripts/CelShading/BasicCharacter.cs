﻿using UnityEngine;


public class BasicCharacter : MonoBehaviour
{
    protected Animator _characterAnim;
    protected readonly float _moveSpeedMax = 3.0f;
    protected readonly float _moveSpeedSmoothTime = 0.075f;
    protected Vector2 _moveSpeedCurrent;
    protected Vector2 _moveSpeedTarget;
    protected Vector2 _moveSpeedVelocity;

    protected virtual void Awake()
    {
        _characterAnim = GetComponent<Animator>();
    }
    protected virtual void Update()
    {
        Vector2 frameMovement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        _moveSpeedTarget = frameMovement.normalized * _moveSpeedMax;
        _moveSpeedCurrent = Vector2.SmoothDamp(_moveSpeedCurrent, _moveSpeedTarget, ref _moveSpeedVelocity, _moveSpeedSmoothTime, float.PositiveInfinity, Time.deltaTime);

        _characterAnim.SetFloat("CurrentMoveSpeed", _moveSpeedCurrent.magnitude);
        Rotate(frameMovement);
    }
    protected virtual void Rotate(Vector2 frameMovement)
    {
        if (frameMovement != Vector2.zero)
        {
            transform.rotation = Quaternion.Euler(0, Mathf.Atan2(-_moveSpeedCurrent.y, _moveSpeedCurrent.x) * Mathf.Rad2Deg - 90.0f, 0);
        }
    }
}
