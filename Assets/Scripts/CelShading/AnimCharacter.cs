﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimCharacter : BasicCharacter
{
    [SerializeField]
    private Fake3DSprite _fakeSprite;
    [SerializeField]
    private bool _shouldMove = false;

    private GameObject _character;


    protected override void Awake()
    {
        _fakeSprite.Initalize();
        _character = _fakeSprite.Sprite3D;
        _characterAnim = _character.GetComponent<Animator>();
    }

    protected override void Update()
    {
        base.Update();
        if (_shouldMove)
        {
            transform.Translate(-_moveSpeedCurrent * Time.deltaTime);
        }
    }

    protected override void Rotate(Vector2 frameMovement)
    {
        if (frameMovement != Vector2.zero)
        {
            _character.transform.rotation = Quaternion.Euler(0, Mathf.Atan2(-_moveSpeedCurrent.y, _moveSpeedCurrent.x) * Mathf.Rad2Deg - 90.0f, 0);
        }
    }
}