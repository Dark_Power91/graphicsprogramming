﻿using UnityEngine;
public class Fake3DSprite : MonoBehaviour
{
	[SerializeField]
	private GameObject _prefab;

    [SerializeField]
    private Transform _targetPosition;

	[SerializeField]
	private Renderer _renderer;

	private Fake3DSpriteRenderer _sprite3DRenderer;

	public GameObject Sprite3D { get; private set; }

	public void Initalize()
	{
		if (_prefab != null)
		{
            Sprite3D = Instantiate(_prefab, _targetPosition);
			_sprite3DRenderer = Instantiate(Resources.Load<Fake3DSpriteRenderer>("Fake3DSpriteRenderer"), _targetPosition);
			_sprite3DRenderer.Initalize(Sprite3D);
			_renderer.material.SetTexture("_MainTex", _sprite3DRenderer.Texture);
		}
	}
}
