﻿using UnityEngine;
public class Fake3DSpriteRenderer : MonoBehaviour
{
	public RenderTexture Texture { get; private set; }

	[SerializeField]
	private Camera _camera;

	public void Initalize(GameObject targetObject)
	{
		Texture = new RenderTexture(1024, 1024, 24)
		{
			name = targetObject.name + " Renderer",
			filterMode = FilterMode.Bilinear,
			antiAliasing = QualitySettings.antiAliasing > 0 ? QualitySettings.antiAliasing : 1
		};

		_camera.targetTexture = Texture;
        targetObject.transform.SetParent(transform);
    }
}