﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursor : MonoBehaviour
{
    [SerializeField]
    private Texture2D CursorSkin;

    void Start()
    {
        Cursor.SetCursor(CursorSkin, Vector3.zero, CursorMode.Auto);
    }

    void OnDestroy()
    {
        Cursor.SetCursor(null, Vector3.zero, CursorMode.Auto);
    }
}
