﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ObjsTag : MonoBehaviour
{
    [System.Serializable]
    private enum Type
    {
        Sphere,
        Cube,
        Cylinder,
        Capsule
    }
    [SerializeField]
    private Type _ObjType;

    public string ObjType
    {
        get
        {
            switch(_ObjType)
            {
                case Type.Sphere:
                    return "Sphere";
                case Type.Cylinder:
                    return "Cylinder";
                case Type.Cube:
                    return "Cube";
                case Type.Capsule:
                    return "Capsule";
                default:
                    return "";
            }
        }
    }
 }
