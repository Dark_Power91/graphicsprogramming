﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject[] MessyObjs;

    [SerializeField]
    private int ObjsQuantity;

    [SerializeField]
    private Text UITargetText;

    private int ToFindIndex;

    private Camera mainCamera;
    private float vertExtent;
    private float horzExtent;
    private GameObject Target;

    private void Awake()
    {
        mainCamera = Camera.main;
        vertExtent = mainCamera.orthographicSize;
        horzExtent = vertExtent * Screen.width / Screen.height;
        GenerateObjects();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                Debug.Log("Hit: " + hit.transform.gameObject.name);
                StartCoroutine("WaitAndRestart");
            }
        }
    }
    private void GenerateObjects()
    {
        Vector3 pos;
        ToFindIndex = Random.Range(0, MessyObjs.Length);
        pos = new Vector3(Random.Range(-horzExtent, horzExtent), Random.Range(-vertExtent, vertExtent), mainCamera.farClipPlane - 0.5f);
        Target = Instantiate(MessyObjs[ToFindIndex], pos, Random.rotation) as GameObject;
        UITargetText.text = "Find the " + Target.GetComponent<ObjsTag>().ObjType;

        for (int i = 0; i < ObjsQuantity; ++i)
        {
            int next = Random.Range(0, MessyObjs.Length);
            while (next == ToFindIndex)
            {
                next = Random.Range(0, MessyObjs.Length);
            }

            GameObject toSpawn = MessyObjs[next];

            pos = new Vector3(Random.Range(-horzExtent, horzExtent), Random.Range(-vertExtent, vertExtent), Random.Range(mainCamera.nearClipPlane + 1.0f, mainCamera.farClipPlane - 1.0f));

            toSpawn = Instantiate(toSpawn, pos, Random.rotation, transform) as GameObject;
            toSpawn.layer = LayerMask.NameToLayer("Ignore Raycast");
        }


    }
    private void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    private void DestroyTarget()
    {
        Destroy(Target);
    }
    private IEnumerator WaitAndRestart()
    {
        DestroyAllChildren();
        yield return new WaitForSeconds(3);
        DestroyTarget();
        GenerateObjects();
    }
}
