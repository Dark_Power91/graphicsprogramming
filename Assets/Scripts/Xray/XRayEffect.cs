﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class XRayEffect : MonoBehaviour
{
    [SerializeField]
    private Shader ReplacementShader;

    [SerializeField]
    private Color OverDrawColor;
    [SerializeField]
    private Color OcclusionColor;

    [SerializeField]
    private float MinRadius;
    [SerializeField]
    private float MaxRadius;

    private Camera myCamera;
    private bool active = false;

    private void OnValidate()
    {
        Shader.SetGlobalColor("_OverDrawColor", OverDrawColor);
        Shader.SetGlobalColor("_OcclusionColor", OcclusionColor);

        Shader.SetGlobalFloat("_MinRadius", MinRadius);
        Shader.SetGlobalFloat("_MaxRadius", MaxRadius);
    }

    private void Awake()
    {
        myCamera = GetComponent<Camera>();
    }

    private void Update()
    {
        Shader.SetGlobalVector("_MousePosition", Input.mousePosition);
        if (Input.GetButtonDown("Fire2"))
        {
            if (active)
            {
                DeactivateXRay();
                active = false;
            }
            else
            {
                ActivateXRay();
                active = true;
            }
        }
    }

    private void ActivateXRay()
    {
        if(ReplacementShader != null)
        {
            myCamera.SetReplacementShader(ReplacementShader, "");
        }
    }

    private void DeactivateXRay()
    {
        myCamera.ResetReplacementShader();
    }
}
